package com.example.shibe.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shibe.model.ShibeRepo

class ShibeVMFactory(
    private val repo: ShibeRepo
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ShibeViewModel(repo) as T
    }
}