package com.example.shibe.utils

enum class AnimalType {
    SHIBE, CAT, BIRD
}