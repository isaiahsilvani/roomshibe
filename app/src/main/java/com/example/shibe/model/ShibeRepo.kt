package com.example.shibe.model

import android.content.Context
import com.example.shibe.model.local.Animal
import com.example.shibe.model.local.AnimalDB
import com.example.shibe.model.remote.ShibeService
import com.example.shibe.utils.AnimalType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShibeRepo(context: Context) {
    // yeah
    private val shibeService = ShibeService.getInstance()
    private val shibeDao = AnimalDB.getInstance(context).animalDao()

    suspend fun getShibes(): List<Animal> = withContext(Dispatchers.IO) {
        val cachedShibes = shibeDao
            .getAllTypedAnimals(AnimalType.SHIBE)
        return@withContext cachedShibes.ifEmpty {
            //Go to the network
            val remoteDoggos = shibeService.getShibes()
            // Map our list of Strings to a Shibe Entity
            val entities: List<Animal> = remoteDoggos.map {
                Animal(type = AnimalType.SHIBE, image = it)
            }
            //Save us some shibes
            shibeDao.insertAnimal(*entities.toTypedArray())
            // return a list of all shibes.
            return@ifEmpty entities
        }
    }


}